<?php
function dbConnect(){
    try
    {
      return new PDO('mysql:host=michelejjnilab.mysql.db;dbname=michelejjnilab;charset=utf8', 'michelejjnilab', 'Marwamode83');
    }
    catch(Exception $e)
    {
        die('Erreur : '.$e->getMessage());
    }
}

function getprojects(){
    $bdd = dbConnect();

    $response = $bdd->query('SELECT * FROM projets ');

    return $response;
}
function getProjet($projetId){
    $bdd = dbConnect();

    $response = $bdd->prepare ('SELECT * FROM projets WHERE id_projet=:id');

    $response->execute(array('id' => $projetId));


    return $response;
}


function createProjet($lien_projet, $titre, $description, $img, $difficulte, $cout, $temps){
  $bdd = dbConnect();

  $requete = $bdd->prepare('INSERT INTO projets(`lien_projet`, `img_projet`, `titre_projet`, `description_projet`, `difficultee_projet`, `cout_projet`, `temps_projet`)
                            VALUES(:lien_projet, :img, :titre , :description, :difficulte , :cout, :temps)');
  $requete->execute(array(
              'lien_projet' => $lien_projet,
              'img' => $img,
              'titre' => $titre,
              'description' => $description,
              'difficulte' => $difficulte,
              'cout' => $cout,
              'temps' => $temps
              ));


    return $requete;

}


function deleteProjet($projetId){
    $bdd = dbConnect();

  $requete = $bdd->prepare('DELETE FROM projets WHERE id_projet= :id_projet');
  $requete->execute(array(
    		       'id_projet' => $projetId
   ));

    return $requete;
}


function updateProjet($id_projet,$lien_projet, $titre, $description, $img, $difficulte, $cout, $temps){
    $bdd = dbConnect();


    $requete = $bdd->prepare('UPDATE projets SET `lien_projet`=:lien_projet,`img_projet`=:img,`titre_projet`=:titre,`description_projet`=:description,`difficultee_projet`=:difficulte,`cout_projet`=:cout,`temps_projet`=:temps WHERE id_projet=:id_projet');
    $requete->execute(array(
                'lien_projet' => $lien_projet,
                'img' => $img,
                'titre' => $titre,
                'description' => $description,
                'difficulte' => $difficulte,
                'cout' => $cout,
                'temps' => $temps,
                'id_projet' => $id_projet
                ));


    return $response;
}

?>
