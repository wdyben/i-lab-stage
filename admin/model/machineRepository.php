<?php
function dbConnect(){
    try
    {
      return new PDO('mysql:host=michelejjnilab.mysql.db;dbname=michelejjnilab;charset=utf8', 'michelejjnilab', 'Marwamode83');
    }
    catch(Exception $e)
    {
        die('Erreur : '.$e->getMessage());
    }
}

function getMachines(){
    $bdd = dbConnect();

    $response = $bdd->query('SELECT * FROM machines');

    return $response;
}
function getMachine($machineId){
    $bdd = dbConnect();

    $response = $bdd->prepare ('SELECT * FROM machines WHERE id_machine=:id');

    $response->execute(array('id' => $machineId));


    return $response;
}


function createMachine( $type_machine, $nom_machine, $desc_machine, $lien_carac, $dim_piece, $cout_machine,$img){
  $bdd = dbConnect();

  $requete = $bdd->prepare('INSERT INTO machines(`type_machine`, `img_machine`, `nom_machine`, `description_machine`, `dimensions_machine`, `lien_caracteristique`, `cout_utilisation`)
                            VALUES(:type_machine, :img, :nom_machine , :desc_machine, :dim_piecee , :lien_carac, :cout_machine)');
  $requete->execute(array(
              'type_machine' => $type_machine,
              'img' => $img,
              'nom_machine' => $nom_machine,
              'desc_machine' => $desc_machine,
              'dim_piecee' => $dim_piece,
              'lien_carac' => $lien_carac,
              'cout_machine' => $cout_machine
              ));


    return $requete;

}


function deleteMachine($machineId){
    $bdd = dbConnect();

  $requete = $bdd->prepare('DELETE FROM machines WHERE id_machine= :id_machine');
  $requete->execute(array(
    		       'id_machine' => $machineId
   ));

    return $requete;
}

function updateMachine($id_machine,$type_machine, $nom_machine, $desc_machine, $lien_carac, $dim_piece, $cout_machine,$img){
    $bdd = dbConnect();


    $requete = $bdd->prepare('UPDATE machines SET `type_machine`=:type_machine,`img_machine`=:img,`nom_machine`=:nom_machine,`description_machine`=:desc_machine,`dimensions_machine`=:dim_piece,`lien_caracteristique`=:lien_carac,`cout_utilisation`=:cout_machine
       WHERE id_machine=:id_machine');
    $requete->execute(array(
            'type_machine' => $type_machine,
            'img' => $img,
            'nom_machine' => $nom_machine,
            'desc_machine' => $desc_machine,
            'dim_piece' => $dim_piece,
            'lien_carac' => $lien_carac,
            'cout_machine' => $cout_machine,
            'id_machine' => $id_machine

                ));


    return $response;
}


?>
