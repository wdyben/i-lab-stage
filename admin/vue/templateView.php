<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?= $title ?></title>

    <link rel="stylesheet" href="<?= $css ?>" />

      <script type="text/javascript" src="../../tinymce/tinymce.js"></script>
      <script type="text/javascript">
      !--
      tinyMCE.init({
      mode : "textareas",
      valid_elements : "em/i,strike,u,strong/b,div[align],br,#p[align],-ol[type|compact],-ul[type|compact],-li"
      });
      //-->
      </script>
</head>
<body>
  <header>
    <a href="<?= $lien ?>"> <img src="<?= $src ?>" id="fleche_retour" alt="retour">  </a>
    <div id="img_log">
    <a href="" >  <p >  <?=$_SESSION['pseudo']?></p> </a>
    <a href="index.php?page=deconnexion" > <img src="image/deconnexion.png"  alt="retour"> </a>
    </div>

  </header>

    <?= $content ?>

    <footer> </footer>
</body>
</html>
