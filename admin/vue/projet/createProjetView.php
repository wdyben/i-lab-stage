<div class="container">
  <h2> Ajouter un projet </h2>

  <form id="form_create_report" method='post' enctype="multipart/form-data">

  <label for='img'>  Image  </label>
  <input class="item" type='file' name='img'/><br><br>

  <label for='nom_projet'> Nom du projet </label>
  <input class="item" type='text' name='nom_projet'/><br><br>

  <label for='desc_projet'>  Description du projet  </label>
  <textarea class="item" name="desc_projet" rows="8" cols="80" placeholder="MAX 2 lignes"></textarea>


  <label for='lien_projet'>  lien du projet  </label>
  <input class="item" type='text' name='lien_projet'/><br><br>

  <label for='difficulte'>  difficulté </label>
  <select class="item" name="difficulte" >
    <option value="">Selectionnez une difficulté</option>
    <option value="facile">Facile</option>
    <option value="moyen">Moyen</option>
    <option value="difficile">Difficile</option>
  </select><br><br>


  <label for='cout_projet'>  Coût du projet </label>
  <input class="item" type='text' name='cout_projet' placeholder="cout en EUR"/><br><br>

  <label for='temps_projet'>  temps de réalisation du projet  </label>
  <input class="item" type='text' name='temps_projet' placeholder="Nombre d'heures "/><br><br>


  <input type='submit' name='valider' value='valider'/>
  </form>

</div>
