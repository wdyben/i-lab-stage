<?php
@ini_set('display_errors', 'on');

session_start();
$lien="index.php?page=machine/displayMachines";
$src="image/fleche_retour.png";
$title="Delete machine";
$css="css/style_delete_machine.css";
$errors = new ArrayObject();
$machineId =$_GET['id'];

require("model/machineRepository.php");

if(!reportExist($machineId)){
    echo ("cette machine n'existe pas");
    exit();
}

$response = getMachine($machineId);
$row=$response->fetch();

if(isFormValid($errors)){
    //set the deletion in boolean instead of string
    $deletionConfirmed = $_POST['deletion'] == '1' ? true : false;

    if($deletionConfirmed){
        $response = deleteMachine($machineId);
        if($response){
          ob_start();
            echo "<p id='msg_conf'>La machine" . $machineId . "a été bien supprimer</p>";
            $content=ob_get_clean();

            require("vue/templateView.php");die;

        }

    }
    ob_start();
     echo "<p id='msg_conf'>La machine " . $machineId . " n'a pas été supprimer </p>";
     $content=ob_get_clean();

     require("vue/templateView.php");die;

}

ob_start();
displayErrors($errors);

require("vue/machine/deleteMachineView.php");

$content=ob_get_clean();

require("vue/templateView.php");

function getMachineIdFromURI(){
    $monUrl = $_SERVER['REQUEST_URI'];
    $monUrl = explode("/", $monUrl) ;
    $machineId = intval(end($monUrl));

    return $machineId;
}

function reportExist($machineId){
    if($machineId == 0){
        return false;
    }

    return getMachine($machineId)->fetch();
    /*
    if(getArticle($articleId)->fetch()){
        return true;
    }
    return false;*/
}

function isFormValid(ArrayObject $errors){
    if(!variablesAreSet()){
        //$errors->append('Variables are not set');
        return false;
    }


    return true;
}

function variablesAreSet(){
    //Get data. If the user come directly in this page. He is redirected
    if(isset($_POST['deletion'])){
        return true;
    }

    return false;
}


function fieldsArefilled($deletionConfirmed){
    //If the user does not fill all the fields a error message is set and he is redirected
    if(empty($deletionConfirmed)){
        return false;
    }
    return true;
}


function displayErrors($errors){
    foreach ($errors as $error) {
        echo $error . '<br>';
    }
}
