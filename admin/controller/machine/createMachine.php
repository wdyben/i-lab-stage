<?php
@ini_set('display_errors', 'on');

session_start();
$title="create projet";
$css="css/style_create_machine.css";
$lien="index.php?page=machine/displayMachines";
$src="image/fleche_retour.png";

$errors = new ArrayObject();
if (variablesAreSet()) {
  require("model/machineRepository.php");
  //$img = $_POST['img'];
  $nom_machine = $_POST['nom_machine'];
  $type_machine = $_POST['type_machine'];
  $desc_machine = $_POST['desc_machine'];
  $lien_carac = $_POST['lien_carac'];
  $dim_piece = $_POST['dim_piece'];
  $cout_machine = $_POST['cout_machine'];

  $nom_img=$_FILES['img']['name'];
  $extension_img=strtolower(  substr(  strrchr($nom_img, '.')  ,1)  );

  if ($_FILES['img']['error'] > 0) $erreur="Erreur lors du transfert";
  $nom = "../images/machines/".$nom_machine."_img.".$extension_img;

  $resultat = move_uploaded_file($_FILES['img']['tmp_name'],$nom);
   if ($resultat) echo "Transfert réussi";

  $img="images/machines/".$nom_machine."_img.".$extension_img;


   $reponse=createMachine( $type_machine, $nom_machine, $desc_machine, $lien_carac, $dim_piece, $cout_machine,$img);



   header("location:index.php?page=machine/displayMachines") ;die;
}

ob_start();
require("model/machineRepository.php");


require("vue/machine/createMachineView.php");

$content=ob_get_clean();

require("vue/templateView.php");

function isFormValid(ArrayObject $errors){
    if(!variablesAreSet()){
        $errors->append('First time in the page');
        return false;
    }
    $img = $_POST['img'];
    $nom_machine = $_POST['nom_machine'];
    $type_machine = $_POST['type_machine'];
    $desc_machine = $_POST['desc_machine'];
    $lien_carac = $_POST['lien_carac'];
    $dim_piece = $_POST['dim_piece'];
    $cout_machine = $_POST['cout_machine'];


    if(!fieldsArefilled($img, $nom_machine,$type_machine,$desc_machine,$lien_carac,$dim_piece,$cout_machine)){
        $errors->append('All the fields must be filled');
        return false;
    }
    return true;
}

function variablesAreSet(){
    //Get data. If the user come directly in this page. He is redirected
    if(isset($_POST['valider'])){
        return true;
    }

    return false;
}

function fieldsArefilled($date_cr,  $contenu){
    //If the user does not fill all the fields a error message is set and he is redirected
    if(empty($date_cr) OR empty($contenu)){
        return false;
    }
    return true;
}

function displayErrors($errors){
    foreach ($errors as $error) {
        echo $error . '<br>';
    }

}

function getChildIdFromURI(){
    $monUrl = $_SERVER['REQUEST_URI'];
    $monUrl = explode("/", $monUrl) ;
    $authorId = end($monUrl) ;

    return $authorId;
}
