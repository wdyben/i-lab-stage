<?php
@ini_set('display_errors', 'on');

session_start();
$title="Update Machine";
$css="css/style_update_machine.css";
$lien="index.php?page=machine/displayMachines";
$src="image/fleche_retour.png";


$machineId = $_GET['id'];

$errors = new ArrayObject();
require("model/machineRepository.php");
$machine=getMachine($machineId);
$row=$machine->fetch();


if(variablesAreSet()){

  $nom_machine = $_POST['nom_machine'];
  $type_machine = $_POST['type_machine'];
  $desc_machine = $_POST['desc_machine'];
  $lien_carac = $_POST['lien_carac'];
  $dim_piece = $_POST['dim_piece'];
  $cout_machine = $_POST['cout_machine'];

  $nom_img=$_FILES['img']['name'];

  if($_FILES['img']['error'] > 0) {

    updateMachine($machineId,$type_machine, $nom_machine, $desc_machine, $lien_carac, $dim_piece, $cout_machine,$row['img_machine']);

    header("location:index.php?page=machine/displayMachines");die;

}	else {
  $nom_img=$_FILES['img']['name'];
  $extension_img=strtolower(  substr(  strrchr($nom_img, '.')  ,1)  );

  $nom = "../images/machines/".$nom_machine."_img_modif.".$extension_img;

  $resultat = move_uploaded_file($_FILES['img']['tmp_name'],$nom);
   if ($resultat) echo "Transfert réussi";

  $img="images/machines/".$nom_machine."_img_modif.".$extension_img;

    updateMachine($machineId,$type_machine, $nom_machine, $desc_machine, $lien_carac, $dim_piece, $cout_machine,$img);
    header("location:index.php?page=machine/displayMachines");die;
}



}

ob_start();

displayErrors($errors);

require("vue/machine/updateMachineView.php");
$content=ob_get_clean();

require("vue/templateView.php");







function variablesAreSet(){
    //Get data. If the user come directly in this page. He is redirected
    if(isset($_POST['valider'])){
        return true;
    }

    return false;
}



function displayErrors($errors){
    foreach ($errors as $error) {
        echo $error . '<br>';
    }

}
