<?php
@ini_set('display_errors', 'on');

session_start();

$title="update actu";
$css="css/style_actu.css";
$lien="index.php?page=page_accueil";
$src="image/fleche_retour.png";
$errors = new ArrayObject();
if(isFormValid($errors))
{   ob_start();
  $actu = $_POST['actu'];

  $contenu = $_POST['bloc'];
   file_put_contents("../fichiers_actualites/".$actu.".txt", '');
   $file = fopen("../fichiers_actualites/".$actu.".txt", "w");
   fwrite($file, $contenu); // ecrit l'e-mail dans le fichier
   fclose($file); // ferme le fichier

   echo "<p id='msg_conf'>Actualité mise à jour </p>";
   $content=ob_get_clean();
   require("vue/templateView.php");die;
}

ob_start();
displayErrors($errors);
require("vue/actualites/update_Actualite_view.php");

$content=ob_get_clean();
require("vue/templateView.php");


function isFormValid(ArrayObject $errors){
    if(!variablesAreSet()){
        $errors->append('');
        return false;
    }
    $actu = $_POST['actu'];

    $contenu = $_POST['bloc'];


    if(!fieldsArefilled($actu, $contenu)){
        $errors->append('All the fields must be filled');
        return false;
    }
    return true;
}

function variablesAreSet(){
    //Get data. If the user come directly in this page. He is redirected
    if(isset($_POST['actu']) AND isset($_POST['bloc'])){
        return true;
    }

    return false;
}

function fieldsArefilled($actu, $contenu){
    //If the user does not fill all the fields a error message is set and he is redirected
    if(empty($actu) OR empty($contenu)){
        return false;
    }
    return true;
}

function displayErrors($errors){
    foreach ($errors as $error) {
        echo $error . '<br>';
    }

}
