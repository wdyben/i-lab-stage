<?php
@ini_set('display_errors', 'on');

$title="Update Projet";
$css="../../css/style_update_projet.css";
$lien="../../projets";
$src="../../image/fleche_retour.png";
session_start();

$projetId = getProjetIdFromURI();

$errors = new ArrayObject();
require("../Model/projetRepository.php");
$projet=getProjet($projetId);
$row=$projet->fetch();


if(variablesAreSet()){

  $nom_projet = $_POST['nom_projet'];
  $desc_projet = $_POST['desc_projet'];
  $lien_projet = $_POST['lien_projet'];
  $dificulte = $_POST['difficulte'];
  $cout_projet = $_POST['cout_projet'];
  $temps_projet = $_POST['temps_projet'];

  if($_FILES['img']['error'] > 0) {

    updateProjet($projetId,$lien_projet, $nom_projet, $desc_projet, $row['img_projet'], $dificulte, $cout_projet,$temps_projet);
    header("location:../projets");die;

}	else {
  $nom_img=$_FILES['img']['name'];
  $extension_img=strtolower(  substr(  strrchr($nom_img, '.')  ,1)  );

  $nom = "../../../images/projets/".$nom_projet."_img_modif.".$extension_img;

  $resultat = move_uploaded_file($_FILES['img']['tmp_name'],$nom);
   if ($resultat) echo "Transfert réussi";

  $img="images/projets/".$nom_projet."_img_modif.".$extension_img;

    updateProjet($projetId,$lien_projet, $nom_projet, $desc_projet, $img, $dificulte, $cout_projet,$temps_projet);
    header("location:../projets");die;
}



}

ob_start();

displayErrors($errors);

require("../vue/projet/updateProjetView.php");
$content=ob_get_clean();

require("../vue/templateView.php");

function getProjetIdFromURI(){
    $monUrl = $_SERVER['REQUEST_URI'];
    $monUrl = explode("/", $monUrl) ;
    $projetId = intval(end($monUrl));

    return $projetId;
}

function reportExist($reportId){
    if($reportId == 0){
        return false;
    }

    return getReport($reportId)->fetch();
    /*
    if(getArticle($articleId)->fetch()){
        return true;
    }
    return false;*/
}


function isFormValid(ArrayObject $errors){
    if(!variablesAreSet()){
        $errors->append('First time in the page');
        return false;
    }
    $date_cr = $_POST['date_cr'];
    $id_enfant = $_POST['id_enfant'];
    $contenu = $_POST['contenu'];


    if(!fieldsArefilled($date_cr,$id_enfant,$contenu)){
        $errors->append('All the fields must be filled');
        return false;
    }
    return true;
}

function variablesAreSet(){
    //Get data. If the user come directly in this page. He is redirected
    if(isset($_POST['valider'])){
        return true;
    }

    return false;
}

function fieldsArefilled($date_cr,$id_enfant,$contenu){
    //If the user does not fill all the fields a error message is set and he is redirected
    if(empty($date_cr) OR empty($id_enfant) OR empty($contenu)){
        return false;
    }
    return true;
}

function displayErrors($errors){
    foreach ($errors as $error) {
        echo $error . '<br>';
    }

}
