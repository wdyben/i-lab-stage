<?php
@ini_set('display_errors', 'on');

session_start();

$title="Delete projet";
$css="css/style_delete_projet.css";
$errors = new ArrayObject();
$projetId = $_GET['id'];
$lien="index.php?page=projet/display_projets";
$src="image/fleche_retour.png";

require("model/projetRepository.php");

if(!reportExist($projetId)){
    echo ("Ce projet n'existe pas ");
    exit();
}



$response = getProjet($projetId);
$row=$response->fetch();
//$lien="../../reports/".$row['id_enfant'];
//$src="../../image/fleche_retour.png";
if(isFormValid($errors)){
    //set the deletion in boolean instead of string
    $deletionConfirmed = $_POST['deletion'] == '1' ? true : false;

    if($deletionConfirmed){
        $response = deleteProjet($projetId);
        if($response){
          ob_start();
            echo "<p id='msg_conf'>Le projet " . $projetId . " a été bien supprimer</p>";
            $content=ob_get_clean();

            require("vue/templateView.php");die;

        }

    }
    ob_start();
    echo "<p id='msg_conf'>Le projet " . $projetId . " n'a pas été supprimer </p>";
    $content=ob_get_clean();

    require("vue/templateView.php");die;



}

ob_start();
displayErrors($errors);

require("vue/projet/deleteProjetView.php");

$content=ob_get_clean();

require("vue/templateView.php");

function getProjetIdFromURI(){
    $monUrl = $_SERVER['REQUEST_URI'];
    $monUrl = explode("/", $monUrl) ;
    $projetId = intval(end($monUrl));

    return $projetId;
}

function reportExist($projetId){
    if($projetId == 0){
        return false;
    }

    return getProjet($projetId)->fetch();
    /*
    if(getArticle($articleId)->fetch()){
        return true;
    }
    return false;*/
}

function isFormValid(ArrayObject $errors){
    if(!variablesAreSet()){
        //$errors->append('Variables are not set');
        return false;
    }


    return true;
}

function variablesAreSet(){
    //Get data. If the user come directly in this page. He is redirected
    if(isset($_POST['deletion'])){
        return true;
    }

    return false;
}


function fieldsArefilled($deletionConfirmed){
    //If the user does not fill all the fields a error message is set and he is redirected
    if(empty($deletionConfirmed)){
        return false;
    }
    return true;
}


function displayErrors($errors){
    foreach ($errors as $error) {
        echo $error . '<br>';
    }
}
