<?php
@ini_set('display_errors', 'on');

session_start();
$title="create projet";
$css="css/style_create_projet.css";
$lien="index.php?page=projet/display_projets";
$src="image/fleche_retour.png";

$errors = new ArrayObject();
if (variablesAreSet()) {
  require("model/projetRepository.php");
  //$img = $_POST['img'];
  $nom_projet = $_POST['nom_projet'];
  $desc_projet = $_POST['desc_projet'];
  $lien_projet = $_POST['lien_projet'];
  $dificulte = $_POST['difficulte'];
  $cout_projet = $_POST['cout_projet'];
  $temps_projet = $_POST['temps_projet'];

  $nom_img=$_FILES['img']['name'];
  $extension_img=strtolower(  substr(  strrchr($nom_img, '.')  ,1)  );

  if ($_FILES['img']['error'] > 0) $erreur="Erreur lors du transfert";
  $nom = "../images/projets/".$nom_projet."_img.".$extension_img;

  $resultat = move_uploaded_file($_FILES['img']['tmp_name'],$nom);
   if ($resultat) echo "Transfert réussi";

  $img="images/projets/".$nom_projet."_img.".$extension_img;


   $reponse=createProjet( $lien_projet, $nom_projet, $desc_projet, $img, $dificulte, $cout_projet,$temps_projet);



   header("location:index.php?page=projet/display_projets") ;die;
}

ob_start();
require("model/projetRepository.php");


require("vue/projet/createProjetView.php");

$content=ob_get_clean();

require("vue/templateView.php");

function isFormValid(ArrayObject $errors){
    if(!variablesAreSet()){
        $errors->append('First time in the page');
        return false;
    }
    //$img = $_POST['img'];
    $nom_projet = $_POST['nom_projet'];
    $desc_projet = $_POST['desc_projet'];
    $lien_projet = $_POST['lien_projet'];
    $dificulte = $_POST['dificulte'];
    $cout_projet = $_POST['cout_projet'];
    $temps_projet = $_POST['temps_projet'];


    if(!fieldsArefilled($img, $nom_machine,$type_machine,$desc_machine,$lien_carac,$dim_piece,$cout_machine)){
        $errors->append('All the fields must be filled');
        return false;
    }
    return true;
}

function variablesAreSet(){
    //Get data. If the user come directly in this page. He is redirected
    if(isset($_POST['valider'])){
        return true;
    }

    return false;
}

function fieldsArefilled($date_cr,  $contenu){
    //If the user does not fill all the fields a error message is set and he is redirected
    if(empty($date_cr) OR empty($contenu)){
        return false;
    }
    return true;
}

function displayErrors($errors){
    foreach ($errors as $error) {
        echo $error . '<br>';
    }

}

function getChildIdFromURI(){
    $monUrl = $_SERVER['REQUEST_URI'];
    $monUrl = explode("/", $monUrl) ;
    $authorId = end($monUrl) ;

    return $authorId;
}
