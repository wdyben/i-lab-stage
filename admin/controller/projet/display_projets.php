<?php
@ini_set('display_errors', 'on');

session_start();

$title="Display projects";
$css="css/style_projets.css";
$lien="index.php?page=page_accueil";
$src="image/fleche_retour.png";
require("model/projetRepository.php");

$response = getprojects();

ob_start();

if(reportExist($response)){


        require("vue/projet/displayProjetsView.php");
        $content=ob_get_clean();

        require("vue/templateView.php");


}else
{


   echo 'Sorry there is no report for' ;
    $content=ob_get_clean();
    require("vue/templateView.php");


}

$response->closeCursor();



function getChildIdFromURI(){
    $monUrl = $_SERVER['REQUEST_URI'];
    $monUrl = explode("/", $monUrl) ;
    $authorId = end($monUrl) ;

    return $authorId;
}
function reportExist($response){
    if($response->rowCount() > 0 ){
        return true;
    }
    return false;

}
