<?php
@ini_set('display_errors', 'on');

$title="Update Machine";
$css=".css/style_update_machine.css";
$lien="index.php?page=machine/displayMachines";
$src="image/fleche_retour.png";
session_start();

$machineId =$_GET['id'];

$errors = new ArrayObject();
require("model/machineRepository.php");
$machine=getMachine($machineId);
$row=$machine->fetch();


if(variablesAreSet()){

  $nom_machine = $_POST['nom_machine'];
  $type_machine = $_POST['type_machine'];
  $desc_machine = $_POST['desc_machine'];
  $lien_carac = $_POST['lien_carac'];
  $dim_piece = $_POST['dim_piece'];
  $cout_machine = $_POST['cout_machine'];

  $nom_img=$_FILES['img']['name'];

  if($_FILES['img']['error'] > 0) {

    updateMachine($id_machine,$type_machine, $nom_machine, $desc_machine, $lien_carac, $dim_piece, $cout_machine,$row['img_machine'])
    header("location:index.php?page=machine/displayMachines");die;

}	else {
  $nom_img=$_FILES['img']['name'];
  $extension_img=strtolower(  substr(  strrchr($nom_img, '.')  ,1)  );

  $nom = "../images/machines/".$nom_machine."_img_modif.".$extension_img;

  $resultat = move_uploaded_file($_FILES['img']['tmp_name'],$nom);
   if ($resultat) echo "Transfert réussi";

  $img="images/machines/".$nom_machine."_img_modif.".$extension_img;

    updateMachine($id_machine,$type_machine, $nom_machine, $desc_machine, $lien_carac, $dim_piece, $cout_machine,$img)
    header("location:index.php?page=machine/displayMachines");die;
}



}

ob_start();

displayErrors($errors);

require("../vue/projet/updateProjetView.php");
$content=ob_get_clean();

require("../vue/templateView.php");

function getProjetIdFromURI(){
    $monUrl = $_SERVER['REQUEST_URI'];
    $monUrl = explode("/", $monUrl) ;
    $projetId = intval(end($monUrl));

    return $projetId;
}

function reportExist($reportId){
    if($reportId == 0){
        return false;
    }

    return getReport($reportId)->fetch();
    /*
    if(getArticle($articleId)->fetch()){
        return true;
    }
    return false;*/
}


function isFormValid(ArrayObject $errors){
    if(!variablesAreSet()){
        $errors->append('First time in the page');
        return false;
    }
    $date_cr = $_POST['date_cr'];
    $id_enfant = $_POST['id_enfant'];
    $contenu = $_POST['contenu'];


    if(!fieldsArefilled($date_cr,$id_enfant,$contenu)){
        $errors->append('All the fields must be filled');
        return false;
    }
    return true;
}

function variablesAreSet(){
    //Get data. If the user come directly in this page. He is redirected
    if(isset($_POST['valider'])){
        return true;
    }

    return false;
}

function fieldsArefilled($date_cr,$id_enfant,$contenu){
    //If the user does not fill all the fields a error message is set and he is redirected
    if(empty($date_cr) OR empty($id_enfant) OR empty($contenu)){
        return false;
    }
    return true;
}

function displayErrors($errors){
    foreach ($errors as $error) {
        echo $error . '<br>';
    }

}
