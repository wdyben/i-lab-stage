
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Les Projets</title>
    <link rel="icon" type="image/gif" href="images/logo2-hdr.png" />
    <link href="css/projets_css.css" rel="stylesheet" />
    <link href="css/navbar.css" rel="stylesheet" />
    <link href="css/footer.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <script src="https://use.fontawesome.com/fb33ff6857.js"></script>

</head>
<body>
  <header id="header">
    <?php  	require("connexion_BD.php"); ?>
    <?php  	require("navbar.php");?>
   </header>

   <section id="block1_conteneur">
     <div class="div_row">

     <?php
     $reponse=getprojects();
      while (  $projet = $reponse->fetch())
    {?>
        <div class="div_projet">
            <a href="<?= $projet['lien_projet']?>" target="_blank">
            <div class="div_img">
              <img class="img_projet2" src="<?= $projet['img_projet']?>" alt="projet_m&n">
            </div>

            <div class="description_projet1">
              <h2><?= $projet['titre_projet']?></h2>
              <p><?= $projet['description_projet']?></p>
              <div class="div_liste">
                  <img src="images/projets/briefcase.png" alt="">  <?= $projet['difficultee_projet']?>
                  <img src="images/projets/money.png" alt=""> <?= $projet['cout_projet']?> EUR
                  <img src="images/projets/hourglass.png" alt=""> <?= $projet['temps_projet']?> heures
              </div>
            </div>
            </a>
        </div>

    <?php  }  ?>


  </div>
    </section>

    <footer>
      <?php  	require("footer.php");?>
    </footer>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
    <script src="js/scroll.js"></script>

</body>
</html>
