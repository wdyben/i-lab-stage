<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>la charte des Fablabs</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <link rel="icon" type="image/gif" href="images/logo2-hdr.png" />
    <link href="css/charte_fablabs_css.css" rel="stylesheet" />
    <link href="css/navbar.css" rel="stylesheet" />
    <link href="css/footer.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
</head>
<body>
  <header id="header">
    <?php  	require("navbar.php"); ?>
  </header>

  <section id="block1_conteneur">
      <div class="div1_section">
          <img src="images/logoFabLab_medium.png" alt="">
          <h2>Afin de porter l’appellation de «Fablab»,
            la structure doit respecter la charte des Fablab,<a href="http://web.mit.edu/">mise en place par le MIT.</a>
          </h2>
      </div>

      <div class="div2_section">
          <h3>Qu’est-ce qu’un Fablab ?</h3>
          <p>
            Les Fablabs sont un réseau mondial de laboratoires locaux,
            qui dopent l’inventivité en donnant accès à des outils de fabrication numérique.
          </p>

          <h3>Qu’est-ce qu’il y a dans un Fablab ?</h3>
          <p>
            Un Fablab mutualise un ensemble de ressources permettant de fabriquer
            à peu près tout ce que l’on veut et de diffuser des connaissances,
            des savoir-faire et des projets.
          </p>

          <h3>Que fourni le réseau des Fablabs ?</h3>
          <p>
            Une assistance opérationnelle, d’éducation, technique,
            financière et logistique au delà de ce qui est disponible dans un seul lab.
          </p>

          <h3>Qui peut utiliser un Fablab ?</h3>
          <p>
           Les Fablabs sont disponibles comme une ressource communautaire,
           qui propose un accès libre aux individus autant qu’un accès sur
           inscription dans le cadre de programmes spécifiques.
          </p>

          <h3>Quelles sont vos responsabilités ?</h3>
          <p>
            Sécurité : Ne blesser personne et ne pas endommager l’équipement.
            Fonctionnement : Aider à nettoyer, maintenir et améliorer le Lab.
            Connaissances : Contribuer à la documentation et aux connaissances des autres
          </p>

          <h3>Qui possède les inventions faites dans un Fablab ?</h3>
          <p>
            Les designs et les procédés développés dans les Fablabs peuvent être protégés
            et vendus comme le souhaite leur inventeur,
            mais doivent rester disponibles de manière à ce que les individus puissent les utiliser et en apprendre.
           </p>

          <h3>Comment les entreprises peuvent utiliser un Fablab ?</h3>
          <p>
           Les activités commerciales peuvent être prototypées et incubées dans un Fablab,
           mais elles ne doivent pas entrer en conflit avec les autres usages,
           elles doivent croître au delà du Lab plutôt qu’en son sein,
           et il est attendu qu’elles bénéficient à leurs inventeurs, aux Labs,
           et aux réseaux qui ont contribué à leur succès.
          </p>
      </div>
  </section>

  <footer>
    <?php  	require("footer.php"); ?>

  </footer>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
  <script src="js/scroll.js"></script>

</body>
</html>
