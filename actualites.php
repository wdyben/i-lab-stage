<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Les actualités</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <link rel="icon" type="image/gif" href="images/logo2-hdr.png" />
    <link href="css/actualites_css.css" rel="stylesheet" />
    <link href="css/navbar.css" rel="stylesheet" />
    <link href="css/footer.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
</head>
<body>
    <header id="header">
        <?php  	require("navbar.php"); ?>

        <!--  <div id="div_header">
          <img id="img1_header" src="images\img1_parc_m.jpg" alt="">
          <img id="img2_header" src="images\img2_parc_m.jpg" alt="">
        </div>-->
    </header>

    <section id="block1_conteneur">

      <div class="div_row">
        <div class="div_projet">
          <?php
              $lines = file("fichiers_actualites/actu1.txt"); // display file line by line
              foreach($lines as $line_num => $line)
               {
                 echo "".$line."<br />\n";
                }
         ?>        </div>

        <div class="div_projet">
          <?php
              $lines = file("fichiers_actualites/actu2.txt"); // display file line by line
              foreach($lines as $line_num => $line)
               {
                 echo "".$line."<br />\n";
                }
         ?>
        </div>

      </div>
    </section>

    <footer>
      <?php  	require("footer.php"); ?>
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
    <script src="js/scroll.js"></script>

</body>
</html>
