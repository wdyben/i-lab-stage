<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Les fondateurs</title>
    <link rel="icon" type="image/gif" href="images/logo2-hdr.png" />
    <link href="css/les_fondateurs_css.css" rel="stylesheet" />
    <link href="css/navbar.css" rel="stylesheet" />
    <link href="css/footer.css" rel="stylesheet" />
    <meta name="viewport" content="width=device-width">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
</head>
<body>

  <header id="header">
      <?php  	require("navbar.php"); ?>
  </header>

  <section id="block1_conteneur">
        <h1>i-lab,un fablab collectif</h1>
        <h2>3 fondateurs pour 1 lieu partagé</h2>
        <div class="div1_section">
            <h3>L'originalité du fablab de Toulon est qu'il a été mise en place et est actuellement porté par 3 structures partenaires :</h3>
            <p>
              <a href="https://kedge.edu/"><em>Kedge Business School</em></a>  propose une offre de formations en management mais également une filière design avec KEDGE Design School.
            </p>
            <p>
              <a href="https://www.isen-mediterranee.fr/fr/actualites/s-epanouir-en-prepa-c-est-possible_nws66_0f94.htm?id=66&gclid=Cj0KCQjwnqzWBRC_ARIsABSMVTPgXiQz7AELZVyE9VtGNBR5pN68og6riheoYCyklnIoMzpYAX4RGYwaAkSIEALw_wcB"><em>L'ISEN,</em></a> école d'ingénieurs généralistes forme des ingénieurs en haute technologie, en informatique et en électronique
            </p>
            <p>
              <a href="http://www.tvt.fr/"><em>TVT Innovation </em></a> s’engage depuis plus de 25 ans à inspirer, accompagner et accélérer les porteurs de projets innovants sur le territoire.
            </p>
        </div>

        <div class="div2_section">
         <a href="http://www.tvt.fr/" target="_blank"><img class="logo_img" src="images/logotvt2.gif" alt="logotvt"></a>
         <a href="https://www.isen-mediterranee.fr/fr/actualites/s-epanouir-en-prepa-c-est-possible_nws66_0f94.htm?id=66&gclid=Cj0KCQjwnqzWBRC_ARIsABSMVTPgXiQz7AELZVyE9VtGNBR5pN68og6riheoYCyklnIoMzpYAX4RGYwaAkSIEALw_wcB" target="_blank">
          <img class="logo_img" src="images/logoisen2.gif" alt="logoisen2">
         </a>
         <a href="https://kedge.edu/" target="_blank"> <img class="logo_img" src="images/logoKBS2.gif" alt="logoKBS2"></a>
      </div>
    </section>

    <footer>
      <?php  	require("footer.php"); ?>
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
    <script src="js/scroll.js"></script>

</body>
</html>
