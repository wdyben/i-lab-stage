<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Contact</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <link rel="icon" type="image/gif" href="images/logo2-hdr.png" />
    <link href="css/contact_css.css" rel="stylesheet" />
    <link href="css/navbar.css" rel="stylesheet" />
    <link href="css/footer.css" rel="stylesheet" />

    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
</head>
<body>
    <header id="header">
      <?php  	require("navbar.php"); ?>
    </header>

    <section id="block1_conteneur">
        <h1> Contactez-nous</h1>
        <h2>Besoin d'information, de conseil ?</h2>

        <div class="div_container">
            <div class="div_contact">
                <em>Eric Serre, </em>
                <p>Responsable technique du Fab Lab vous conseille.</p>
                <form class="form" action="mail.php" method="post">
                    <div class="msg_confirmation">
                      <?php   displayErrors($errors); ?>
                      <p><?php echo $error; ?></p>
                    </div>
                    <input class="item" placeholder="Nom" type="text" name="nom">
                    <input class="item" placeholder="Votre e-mail" type="email" name="e-mail" >
                    <input class="item" placeholder="Sujet" type="text" name="sujet" >
                    <textarea class="item" name="msg" placeholder="Votre message" rows="8" cols="80"></textarea>
                    <input id="boutton_envoyer" type="submit" name="envoyer" value="Envoyer">
                </form>
            </div>

            <div class="div_contact">
                <h3>Notre adresse </h3>
                <p >
                  <a href="https://www.google.fr/maps/place/Maison+du+Num%C3%A9rique+et+de+l'Innovation/@43.1207265,5.9368238,17z/data=!3m1!4b1!4m5!3m4!1s0x12c91b0a4567288b:0x6a8c785cb1fd4981!8m2!3d43.1207226!4d5.9390125" target="_blank" title="Géolocalisez nous" rel="external nofollow">
                    <div id="mapPic"><img src="images/map.png" alt="Géolocalisez la Cantine TVT"></div>

                  </a>
                </p>
                <p>  Maison du Numérique et de l'Innovation
                  Place G. Pompidou
                  83000 Toulon
                </p>
            </div>

            <div class="div_contact">
              <h3>Nos horaires </h3>
              <div class="div_horaire">
                <p><strong>LUNDI </strong>&nbsp;	Fermé</p>
                <p><strong>MARDI </strong>&nbsp;	9h–12h30 sur rendez-vous	14h–18h</p>
                <p><strong>MERCREDI </strong>	&nbsp;9h–12h30	14h-18h</p>
                <p><strong>JEUDI </strong>&nbsp;	9h–12h30 sur rendez-vous	14h–18h</p>
                <p><strong>VENDREDI </strong>&nbsp;	14h–18h	Open Lab 18h–21h</p>
              </div>

              <div class="div_RS">
                <h3>Retrouvez-nous sur :</h3>
                <a href="https://www.facebook.com/ilab.toulon/" target="_blank"><img class="img_fb" src="images/icon_fb.png" alt="facebook"></a>
                <a href="https://www.instagram.com/ilab_toulon/?hl=fr" target="_blank"><img class="img_fb" src="images/icon_insta.png" alt="insta"></a>
              </div>

              <div class="div_RS">
                  <h3>Tél. 06 18 07 63 33</h3>
              </div>
            </div>
        </div>
    </section>

   <footer>
     <?php  	require("footer.php"); ?>

   </footer>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
   <script src="js/scroll.js"></script>

</body>
</html>
