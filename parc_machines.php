
  <!DOCTYPE html>
  <html>
  <head>
      <meta charset="utf-8" />
      <title>Parc Machines</title>
      <link rel="icon" type="image/gif" href="images/logo2-hdr.png" />
      <link href="css/parc_machines_css.css" rel="stylesheet" />
      <link href="css/navbar.css" rel="stylesheet" />
      <link href="css/footer.css" rel="stylesheet" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
      <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
  </head>
  <body>
      <header id="header">
        <?php  	require("connexion_BD.php"); ?>
          <?php  	require("navbar.php"); ?>
      </header>

      <section id="block1_conteneur">
        <?php
        $i=1;
        $reponse=getmachines();
        while ($machine = $reponse->fetch()) {
          if ($i==1) {
            if ($machine['type_machine']=="Imprimante 3D à dépôt de fil") {
              ?>
                <h2><?= $machine['type_machine']?></h2>
                <p>Le fablab dispose de 2 modèles d'imprimantes 3D à dépôt de fil :</p>
                <p class="p_cout"><?= $machine['cout_utilisation']?></p>
                <div class="div">
                  <img class="img_machine" src="<?= $machine['img_machine']?>" alt="<?= $machine['nom_machine']?>">
                  <div class="div1_3d">
                    <h3><?= $machine['nom_machine']?></h3>
                      <p><?= $machine['description_machine']?></p>
                      <p><?= $machine['dimensions_machine']?></p>
                      <a href="<?= $machine['lien_caracteristique']?>" target="_blank"> Caractéristiques techniques </a>

                  </div>
                </div>
              <?php $i=0;
            } else {
                  ?>
                    <h2><?= $machine['type_machine']?></h2>
                    <div class="div">
                      <img class="img_machine" src="<?= $machine['img_machine']?>" alt="<?= $machine['nom_machine']?>">
                      <div class="div1_3d">
                        <h3><?= $machine['nom_machine']?></h3>
                          <p><?= $machine['description_machine']?></p>
                          <p><?= $machine['dimensions_machine']?></p>
                          <a href="<?= $machine['lien_caracteristique']?>" target="_blank"> Caractéristiques techniques </a>
                          <p class="p_cout">
                            <?= $machine['cout_utilisation']?>
                          </p>
                      </div>
                    </div>
              <?php   $i=0;
            }

          } else {
            ?>
              <h2><?= $machine['type_machine']?></h2>
              <div class="div div_decaler">
                <div class="div2_3d">
                  <h3><?= $machine['nom_machine']?></h3>
                  <p>
                   <?= $machine['description_machine']?></p>
                  <p> <?= $machine['dimensions_machine']?>
                  </p>

                  <a href="<?= $machine['lien_caracteristique']?>" target="_blank"> Caractéristiques techniques</a>
                  <p class="p_cout">
                    <?= $machine['cout_utilisation']?>
                  </p>
                </div>

                <img class="img_machine" src="<?= $machine['img_machine']?>" alt="<?= $machine['nom_machine']?>">
              </div>
            <?php
            $i=1;
          }
        } ?>


        <h2>Matériel électronique</h2>
          <div class="div">
            <img src="images/machines/electro_large.jpg" alt="electro">
            <div class="div3_3d">
                <p>
                  Le fablab dispose de tout le matériel utile pour prototyper de l'électronique embarqué.
                   A l'heure où chacun souhaite connecter les objets qui l'entourent,
                   i-lab met à disposition des fers à souder,
                   certains composants électroniques mais également des cartes Arduino et des capteurs ou micro-contrôleurs pour réaliser les premiers montages électroniques.
                </p>
            </div>

          </div>

      </section>

      <footer>
        <?php  	require("footer.php"); ?>
      </footer>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
      <script src="js/scroll.js"></script>

  </body>
  </html>
