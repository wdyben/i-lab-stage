<?php

$errors = new ArrayObject();
if(isFormValid($errors)){
  $nom = $_POST['nom'];
  $mail = $_POST['e-mail'];
  $msg = $_POST['msg'];
   $sujet=$_POST['sujet'];
  if (filter_var($mail, FILTER_VALIDATE_EMAIL))
    {
            $to = 'fablab@tvt.fr';
            $objet="Formulaire de contact Site du Fablab";
            $msg="Vous avez recu un nouveau message du site \n
                    Nom : $nom \r\n
                    Email : $mail \r\n
                    Sujet :  $sujet \r\n
                    Message : $msg";

            $entete="From: $nom \n Reply-To: $mail";
          	$entete.="Content-Type: text/plain; charset=utf-8";

              if(mail($to,$objet,$msg,$entete))
              {
                $error="Votre mail a été bien envoyer";
                   require("contact.php");

              }else
              {
                $error="Votre mail n'a pas été envoyer , Veuillez réesayer ";
                require("contact.php");
              }
    }else
    {
      $error="Votre adresse mail est invalide , Veuillez réesayer ";
      require("contact.php");
    }


  }else {
    $error="";
    require("contact.php");
  }



  function isFormValid(ArrayObject $errors)
  {
      if(!variablesAreSet()){
          //$errors->append('First time in the page');
          return false;
      }
      $nom = $_POST['nom'];
      $mail = $_POST['e-mail'];
      $sujet = $_POST['sujet'];

      $msg = $_POST['msg'];

      if(!fieldsArefilled($nom, $mail,$sujet,$msg))
      {
          $errors->append('All the fields must be filled');
          return false;
      }
      return true;
  }

  function variablesAreSet(){
      //Get data. If the user come directly in this page. He is redirected
      if( (isset($_POST['nom'])) AND (isset($_POST['e-mail'])) AND (isset($_POST['sujet'])) AND (isset($_POST['msg'])) ){
          return true;
      }

      return false;
  }

  function fieldsArefilled($nom, $mail,$sujet,$msg)
  {
      //If the user does not fill all the fields a error message is set and he is redirected
      if( (empty($nom)) OR (empty($mail)) OR (empty($sujet)) OR (empty($msg)) )
      {

          return false;
      }
      return true;
  }


  function displayErrors($errors){
      foreach ($errors as $error) {
          echo $error . '<br>';
      }

  }
             ?>
