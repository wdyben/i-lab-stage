<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
 	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>i-lab</title>
    <link rel="icon" type="image/gif" href="images/logo2-hdr.png" />
    <link href="css/accueil.css" rel="stylesheet" />
    <link href="css/navbar.css" rel="stylesheet" />
    <link href="css/footer.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
</head>
<body>


    <header id="header">
    	<?php  	require("navbar.php"); ?>

      <div align="center">

    <div class="contener_slideshow">
      <div class="div_titre">
        <h1>CREER PARTAGER INNOVER</h1>
        <h2>Votre FabLab au coeur du campus Numérique de Toulon </h2>
        <a href="#block1_conteneur"><img id="arrowdown" src="images/arrowdown.png"></a>

      </div>

      <div class="contener_slide">
        <div class="slid_1"><img src="images/new/slide0.png"></div>
        <div class="slid_2"><img src="images/new/slide1.png"></div>
        <div class="slid_3"><img src="images/new/slide2.png"></div>
        <div class="slid_4"><img src="images/new/slide3.png"></div>
        <div class="slid_5"><img src="images/new/slide4.png"></div>
      </div>
    </div>
  </div>
    </header>
  <section id="block1_conteneur">

    <div id="phrase1" class="quoi_contenue">
     <h2><strong><mark>l'I-lab </mark><br>c'est quoi ?</strong> </h2>
    </div>
    <div id="descpitif_quoi_conteneur" class="quoi_contenue">

      <div class="descpitif_contenue">
        L’Innovation Lab, ou i-lab, est un lieu de rencontres et de créations collaboratives partagées, au sein de la Maison du Numérique et de l’Innovation, en centre-ville de Toulon. Il offre à la fois des espaces de co-working, de co-design, et un Fab lab.

      <div id="paragraphe_en_gris"><br>
        Le Fab Lab (fabrication laboratory) est un lieu ouvert qui met à disposition toutes sortes d'outils dont les outils de fabrication numérique, machines-outils pilotées par ordinateur, pour la conception et la réalisation d'objets. Ou plus précisément pour permettre de passer de la phase de concept à la phase de prototypage, et de la phase de prototypage à la phase de mise au point. Il a obtenu la labellisation du Massachusetts Institute of Technology (MIT) et respecte la charte des Fab Labs, en permettant aux utilisateurs d’accéder à la technologie, et en favorisant le partage des connaissances et compétences techniques.
      </div>
    </div>
  </section>

  <section id="block2_conteneur">

    <div id="phrase2" class="Aqui_contenue">
     <h2><strong>A qui s'adresse <mark>l'i-lab</mark> ?</strong> </h2>
    </div>
    <div id="descpitif_Aqui_conteneur" class="Aqui_contenue">

      <div class="descpitif_Aqui_contenue">
        <strong><mark>L’i-lab</mark></strong> est un lieu totalement ouvert qui propose selon les formules, un accès libre ou un accès avec inscription pour des ateliers spécifiques. Il s'adresse à tous, entrepreneurs, designers, communautés d’usagers, bricoleurs, étudiants, publics scolaires, particuliers, et même aux hackers en tout genre.
      </div>
    </div>
  </section>



  <section id="block6_conteneur">
    <div id="adresse" class="informations"> Adresse <br><span> <h2>Maison du Numérique et de l’Innovation </h2><br>Place Georges Pompidou 83000 Toulon</span>
    </div>
    <div id="horaire_image" class="informations">
      <div><img src="images/sparkWht-ilab.png"></div>
      <div id="horaire_ouverture">Horaires d'ouverture</div>
      <div id="horaire_colonne">
        <div class="horaire_jour_semaine">
          <div class="horaire_case">
            LUNDI
          </div>

          <div class="horaire_case">
            Fermé
          </div>
          <div class="horaire_case"></div>
        </div>
        <div class="horaire_jour_semaine">
          <div class="horaire_case">
            MARDI
          </div>
          <div class="horaire_case">
            9h–12h30 sur rendez-vous
          </div>
          <div class="horaire_case">
            14h–18h
          </div>
        </div>
        <div class="horaire_jour_semaine">
          <div class="horaire_case">
            MERCREDI
          </div>
          <div class="horaire_case">
            9h–12h30
          </div>
          <div class="horaire_case">
            14h–18h
          </div>
        </div>
        <div class="horaire_jour_semaine">
          <div class="horaire_case">
            JEUDI
          </div>
          <div class="horaire_case">
            9h–12h30 sur rendez-vous
          </div>
          <div class="horaire_case">
            14h–18h
          </div>
        </div>
        <div class="horaire_jour_semaine">
          <div class="horaire_case">
            VENDREDI
          </div>
          <div class="horaire_case">
            14h–18h
          </div>
          <div class="horaire_case">
            Open Lab 18h–21h
          </div>
        </div>
      </div>

    </div>

    <div id="imagemain" class="informations"> <img src="images/ILAB-HANDS1.jpg" width="400px;"></div>

  </section>

  <footer>
    <?php  	require("footer.php"); ?>
  </footer>

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
  <script type="text/javascript" src="js/scroll.js"></script>

</body>
</html>
